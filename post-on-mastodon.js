import 'dotenv/config'
import fs from 'node:fs';
import Mastodon from '@jaller94/mastodon-api';

const M = new Mastodon({
  client_key: process.env.CLIENT_KEY,
  client_secret: process.env.CLIENT_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
  api_url: process.env.API_URL, // optional, defaults to https://mastodon.social/api/v1/
});

async function post() {
  const puzzleImgResp = await M.post('media', { file: fs.createReadStream('puzzle.png') });
  const solutionImgResp = await M.post('media', { file: fs.createReadStream('solution.png') });
  const puzzleParams = {
    language: 'eng',
    status: fs.readFileSync('puzzle-status.txt', 'utf-8').trim(),
    media_ids: [puzzleImgResp.data.id],
    sensitive: false,
    visibility: 'unlisted',
  };
  const puzzlePostResp = await M.post('statuses', puzzleParams);
  const solutionParams = {
    language: 'eng',
    status: fs.readFileSync('solution-status.txt', 'utf-8').trim(),
    media_ids: [solutionImgResp.data.id],
    in_reply_to_id: puzzlePostResp.data.id,
    spoiler_text: fs.readFileSync('solution-status.txt', 'utf-8').trim(),
    sensitive: true,
    visibility: 'unlisted',
  };
  await M.post('statuses', solutionParams);
}

post().catch((err) => {
  console.error(err);
  process.exit(1);
});
