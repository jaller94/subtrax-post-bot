#!/bin/sh

node node_modules/subtrax-tools/src/cli.js || exit 1
inkscape --export-png puzzle.png --export-dpi 900 --export-background white puzzle.svg || exit 3
pngcrush -c 2 puzzle.png puzzle.png || exit 4
inkscape --export-png solution.png --export-dpi 900 --export-background white solution.svg || exit 5
pngcrush -c 2 solution.png solution.png || exit 6

echo "Puzzle for $(date -u +%Y-%m-%d)" > puzzle-status.txt
echo "Solution for $(date -u +%Y-%m-%d)" > solution-status.txt
node post-on-mastodon.js || exit 7

rm puzzle.png
rm puzzle.svg
rm solution.png
rm solution.svg
rm puzzle.json
rm puzzle-status.txt
rm solution-status.txt
